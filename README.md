# Quarrel
A Easy to customise bubbily discord theme with lots of **customisation/flexability** to adjust to your likings.

![preview]()

## How To Install Quarrel?

Its Simple!

For **Powercord** Users:

1. Open CMD & Type:

```
cd powercord/src/Powercord/themes && git clone https://github.com/leeprky/Quarrel
```
# Extras 

Thank you for checking out/downloading my Theme :)
Direct Message Me On @Discord Or Join By **[github.com/leeprky](https://discord.gg/Ff3rqAYB89)** For More Help/Information For Installing/Support. Enjoy :)
